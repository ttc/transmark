/* eslint default-case: [0], consistent-return: [0] */
import _ from 'lodash/fp';
import jsc from 'jsverify';

export const zipperConstructor = count => {
  switch (jsc.random(0, 1)) {
    case 0: { return 'zipper.empty()'; }
    case 1: {
      const list = _.map(() => jsc.random(0, 100), _.range(0, count));
      return `zipper.fromList([${list.join(', ')}])`;
    }
  }
};

export const zipper = (depth = 10, count = 10) => {
  // If we reach this deep nesting, make sure to always return a constructor.
  if (depth === 0) { return zipperConstructor(count); }

  // Recursively construct the symbolic zipper stack as a text string, e.g.:
  // 'zipper.forward(zipper.jumpTo(2, zipper.fromList([42, 23, 80, 3])))'
  switch (jsc.random(0, 5)) {
    case 0: { return zipperConstructor(count); }
    case 1: { return `zipper.forwardOne(${zipper(depth - 1, count)})`; }
    case 2: { return `zipper.backOne(${zipper(depth - 1, count)})`; }
    case 3: {
      const position = jsc.random(0, count);
      return `zipper.jumpTo(${position}, ${zipper(depth - 1, count)})`;
    }
    case 4: { return `zipper.start(${zipper(depth - 1, count)})`; }
    case 5: { return `zipper.end(${zipper(depth - 1, count)})`; }
  }
};

// Model functions, they turn zippers into a list model.
export const pathModel = z => _.reverse(z.path);
export const listModel = z => z.list;
export const model = z => _.concat(pathModel(z), listModel(z));
export const indexModel = z => _.size(pathModel(z));
export const cursorModel = z => _.first(listModel(z));
export const previousCursorModel = z => {
  const path = pathModel(z);
  const list = listModel(z);
  return _.isEmpty(path) ? _.first(list) : _.last(path);
};
