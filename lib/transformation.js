import _ from 'lodash/fp';

import {currentHeading, balanceSection, replaceToken} from './markdown';
import {isAtEnd, clone, forwardOne, start} from './zipper';

const snippetTransform = (metaData, zipper) => {
  const uuid = zipper.cursor.content;
  const headingLevel = currentHeading(zipper);
  const replacement = _.first(metaData.uuid[uuid]);
  return replaceToken(balanceSection(headingLevel, replacement), zipper);
};

// The keys should have the same name as the token type.
export const transformations = {
  snippet: snippetTransform,
};

/** Transform the markdown to the final form.
   @param {Object} metaData - The transmark state received from parse.
   @param {Object} customTransformers - Extend the transmark transformation.
   @param {Zipper} source - A transmark zipper to transform.
   @returns {Zipper}
 */
// TODO: The argument order feels weird, maybe have to change it.
export const transform = _.curry((metaData, customTransformers, source) => {
  let zipper = clone(source);
  const transformers = _.merge(transformations, customTransformers);

  while (!isAtEnd(zipper)) {
    const type = zipper.cursor.type;
    // TODO: Add here support for multiple snippets in a row and to keep the
    //       header balancing right????
    switch (type) {
      case (_.has(type, transformers) ? type : null): {
        zipper = transformers[type](metaData, zipper);
        break;
      }
      default: { break; }
    }
    zipper = forwardOne(zipper);
  }

  return start(zipper);
});

export default {
  transform, transformations,
};
