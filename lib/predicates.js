import _ from 'lodash/fp';

// A slew of different predicates.
// [a] -> b -> Boolean
// const pred = and(isOpenHeading, isTag('h2'));
// pred(token);
export const and = (...ps) =>
  v => _.reduce((memo, p) => {
    const ret = memo ? memo && p(v) : memo;
    return ret;
  }, true, ps);

export const isTokenType = _.curry((type, e) => _.eq(type, e.type));

export const isHeadingOpen = isTokenType('heading_open');
export const isHeadingClose = isTokenType('heading_close');
export const isMetaBlock = isTokenType('meta_block');
export const isParagraphOpen = isTokenType('paragraph_open');
export const isParagraphClose = isTokenType('paragraph_close');
export const isInline = isTokenType('inline');
export const isTag = _.curry((tag, e) => _.eq(e.tag, tag));
export const isTagLevel = _.curry((tag, e) => {
  const level = t => parseInt(t.replace(/^h/, ''), 10);
  return _.lte(level(e.tag), level(tag));
});

export const isHeading = (e) => /^h[1-6]$/.test(e.tag);
export const isFirstLevelHeading = and(isHeadingOpen, isTag('h1'));
export const isSecondLevelHeading = and(isHeadingOpen, isTag('h2'));
export const isSnippet = isTokenType('snippet');

export default {
  and,
  isTokenType, isMetaBlock, isInline, isTag, isTagLevel,
  isHeadingOpen, isHeadingClose, isParagraphOpen, isParagraphClose,
  isHeading, isFirstLevelHeading, isSecondLevelHeading, isSnippet,
};
