/* eslint no-param-reassign: ["error", { "props": false }] */

// The implementation of this meta block plugin for markdown-it is heavily
// based on the [markdown-it-container
// plugin](https://github.com/markdown-it/markdown-it-container).

import _ from 'lodash/fp';

/** Extend markdown-it to parse meta blocks.
   @param {Object} md - The markdown-it parser object.
   @param {Object} options - configuration options for this plugin.
   @param {number} [options.minMarkers=3] - The number or marker strings to
                                            delimit the block.
   @param {string} [options.markerStr=-] - The string to use as a delimiter.
 */
export const markdownMetaBlock = (md, options = {}) => {
  const defaults = {
    minMarkers: 3,
    markerStr: '-',
  };

  const {minMarkers, markerStr} = _.merge(defaults, options);
  const markerChar = markerStr.charCodeAt(0);
  const markerLength = _.size(markerStr);

  const container = (state, startLine, endLine, silent) => {
    let start = state.bMarks[startLine] + state.tShift[startLine];
    let max = state.eMarks[startLine];
    let autoClosed = false;
    let pos;
    let nextLine;

    // Check out the first character, this filters out most non-containers.
    if (markerChar !== state.src.charCodeAt(start)) { return false; }

    // Check out the rest of the marker string
    for (pos = start + 1; pos <= max; pos++) {
      if (markerStr[(pos - start) % markerLength] !== state.src[pos]) {
        break;
      }
    }

    const markerCount = Math.floor((pos - start) / markerLength);

    if (markerCount < minMarkers) { return false; }

    pos -= (pos - start) % markerLength;
    const markup = state.src.slice(start, pos);
    const params = state.src.slice(pos, max);

    // Since start is found, we can report success here in validation mode.
    if (silent) { return true; }

    nextLine = startLine;

    for (;;) {
      nextLine++;
      if (nextLine >= endLine) {
        // unclosed block should be autoclosed by end of document.
        // also block seems to be autoclosed by end of parent
        break;
      }

      start = state.bMarks[nextLine] + state.tShift[nextLine];
      max = state.eMarks[nextLine];

      if (start < max && state.sCount[nextLine] < state.blkIndent) {
        // non-empty line with negative indent should stop the list:
        // - ```
        //  test
        break;
      }

      if (markerChar !== state.src.charCodeAt(start)) { continue; }

      if (state.sCount[nextLine] - state.blkIndent >= 4) {
        // closing fence should be indented less than 4 spaces
        continue;
      }

      for (pos = start + 1; pos <= max; pos++) {
        if (markerStr[(pos - start) % markerLength] !== state.src[pos]) {
          break;
        }
      }

      // closing code fence must be at least as long as the opening one
      if (Math.floor((pos - start) / markerLength) < markerCount) { continue; }

      // make sure tail has spaces only
      pos -= (pos - start) % markerLength;
      pos = state.skipSpaces(pos);

      if (pos < max) { continue; }

      // found!
      autoClosed = true;
      break;
    }

    const oldParent = state.parentType;
    const oldLineMax = state.lineMax;

    state.parentType = 'meta-block';

    // this will prevent lazy continuations from ever going past our end marker
    state.lineMax = nextLine;

    start = state.bMarks[startLine] + state.tShift[startLine];

    const metaData = _(state.src)
      .take(pos - markup.length)
      .drop(start + markup.length + params.length)
      .join('');

    const token = state.push('meta_block', 'div', 1);
    token.markup = markup;
    token.params = _.trim(params, ' ');
    token.block = true;
    token.map = [startLine, nextLine];
    token.content = _.trim(metaData, '\n');

    state.parentType = oldParent;
    state.lineMax = oldLineMax;
    state.line = nextLine + (autoClosed ? 1 : 0);

    return true;
  };

  md.block.ruler.before('fence', 'meta_block', container, {
    alt: ['paragraph', 'reference', 'blockquote', 'list'],
  });
};
