/* eslint no-param-reassign: ["error", { "props": false }] */
import _ from 'lodash/fp';
import yaml from 'js-yaml';

import {forwardOne, forwardUntil, backUntil, start, end, cutOne, cutMany,
        clone, isAtEnd, slice, cutUntil, insert, replace, jumpTo} from './zipper';
import {and,
        isHeadingOpen, isHeadingClose, isMetaBlock,
        isInline, isTagLevel} from './predicates';

/** Parse the contents of a meta block using YAML.
   @param {Zipper} tokens - The markdown tokens zipper.
   @return {Object} The meta data object.
 */
export const meta = tokens => {
  if (_.isNil(tokens.cursor)) { return {}; }
  return yaml.safeLoad(tokens.cursor.content);
};

/** Forward one header.
   @param {Zipper} tokens - The markdonw tokens zipper.
   @returns {Zipper} - The zipper with the cursors moved after the header.
 */
export const forwardByHeader = _.flow([forwardUntil(isHeadingClose),
                                       forwardOne]);

/** Move the markdown token stream one section.
   @param {Zipper} tokens - The markdown tokens zipper.
   @returns {Zipper} - The zipper with the cursor moved by one section.
 */
export const forwardToNextSection = tokens => {
  // The nesting of the section can be only determined by the tag attribute.
  const tag = tokens.cursor.tag;

  if (tag === 'h1') return end(tokens);
  return forwardUntil(and(isHeadingOpen, isTagLevel(tag)), tokens);
};

/** Slice the section of a markdown zipper.
   @param {Zipper} tokens - The zipper to slice.
   @returns {Zipper} - The section that got sliced.
 */
export const sliceSection = tokens => {
  const begin = tokens.index;
  const tag = tokens.cursor.tag;

  if (tag !== 'h1') {
    const until = forwardToNextSection(tokens).index;
    return slice(begin, until, tokens);
  }

  return tokens;
};

/** Slice the heading of a section.
   @param {Zipper} tokens - The zipper to slice.
   @returns {Zipper} - The heading that got sliced.
 */
export const sliceHeading = tokens => {
  const begin = tokens.index;
  // Use +1 to include the element at point as well.
  const until = forwardUntil(isHeadingClose, tokens).index + 1;
  return slice(begin, until, tokens);
};

/** Split the markdown zipper into it's subsections.
   @param {Zipper} tokens - The zipper to split up.
   @returns {Array<Zipper>} - The sub sections of the markdown zipper.
 */
export const subSections = tokens => {
  const ret = [];
  let sections = cutUntil(isHeadingOpen, clone(tokens));

  while (!isAtEnd(sections)) {
    const section = sliceSection(sections);
    ret.push(section);
    sections = cutMany(section.length, sections);
  }

  return ret;
};

/** Balance the section levels towards a new root level.
   @param {string} rootLevel - The section heading tag to use as the root
                               level.
   @param {Zipper} tokens - The markdown token zipper.
   @returns {Zipper} The markdown token zipper with the section heading levels
                     rebalanced.
 */
export const balanceSection = _.curry((rootLevel, tokens) => {
  const trimHeading = tag => _(tag).drop(1).first();
  const parseHeading = _.flow([trimHeading, _.parseInt(10)]);
  const tag = level => `h${level}`;

  const base = parseHeading(rootLevel);
  let current = parseHeading(tokens.cursor.tag);
  let depth = 1;

  let section = clone(tokens);

  while (!isAtEnd(section)) {
    const tokenType = section.cursor.type;

    switch (tokenType) {
      case (isHeadingOpen(section.cursor) || isHeadingClose(section.cursor) ?
            tokenType :
            null): {
              const local = parseHeading(section.cursor.tag);
              if (local > current) {
                depth = depth + 1;
              } else if (local < current) {
                depth = depth - 1;
              }
              section.cursor.tag = tag(base + depth);
              current = local;
              break;
            }
      default: { break; }
    }

    section = forwardOne(section);
  }

  return start(section);
});

/** Insert a markdown token zipper after the current element of another
   zipper.
   @param {Zipper} section - The zipper to insert.
   @param {Zipper} tokens - The target zipper.
   @returns {Zipper} A new zipper with `section` inserted.
 */
export const insertSection = _.curry((section, tokens) =>
  insert(section, forwardOne(tokens)));

/** Replace the current token with another zipper.
   @param {Zipper} section - The section that replaces.
   @param {Zipper} tokens - The zipper to replace the current token.
*/
export const replaceToken = _.curry((section, tokens) =>
  replace(section, tokens));

/** Get the current section title.
   @param {Zipper} tokens - A markdown token zipper.
   @return {string} The title.
 */
export const sectionTitle = tokens =>
  forwardUntil(isInline, tokens).cursor.content;

/** Parse the next meta block.
   @param {Zipper} tokens - A markdown token zipper.
   @returns {Object} A key-value meta object.
 */
export const metaSection = tokens => meta(forwardUntil(isMetaBlock, tokens));

/** Get the current heading level.
   @param {Zipper} tokens - A markdown token zipper.
   @returns {string} The current heading tag.
 */
export const currentHeading = tokens =>
  backUntil(isHeadingOpen, tokens).cursor.tag;

/** Cut out a heading block.
   @param {Zipper} tokens - A markdown token zipper.
   @returns {Zipper} A new version of `tokens` with the heading removed.
 */
export const cutHeading = cutMany(3);

/** Cut out a meta block.
   @param {Zipper} tokens - A markdown token zipper.
   @returns {Zipper} A new version of `tokens` with the meta block removed.
 */
export const cutMeta = cutMany(1);

/** Cut out a header.
   @param {Zipper} tokens - A markdown token zipper.
   @returns {Zipper} A new version of `tokens` with the heading and the meta
                     block removed.
 */
export const cutHeader = _.flow([cutHeading, cutMeta]);

/** Cut out a meta marker.
   @param {Zipper} tokens - A markdown token zipper.
   @returns {Zipper} A new version of `tokens` with the meta marker removed.
 */
export const cutMarker = cutOne;

/** Cut the contents of a section, but keep the header.
   @param {Zipper} tokens - A markdown token zipper.
   @returns {Zipper} A new version of `tokens` with the contents of the section
                     removed.
 */
export const cutSectionContents = tokens => {
  const begin = forwardByHeader(tokens).index;
  const until = forwardToNextSection(tokens).index;
  const cutCount = until - begin;
  return _.flow([jumpTo(begin), cutMany(cutCount)])(tokens);
};

export default {
  meta,
  forwardByHeader, forwardToNextSection,
  cutMeta, cutHeading, cutHeader, cutMarker, cutSectionContents,
  sectionTitle, metaSection, currentHeading,
  sliceSection, sliceHeading, subSections,
  insertSection, replaceToken, balanceSection,
};
