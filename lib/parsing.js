/* eslint no-param-reassign: ["error", { "props": false }],
          import/no-mutable-exports: 1 */
import _ from 'lodash/fp';
import markdownIt from 'markdown-it';

import {markdownMetaBlock} from './markdown-it-meta';
import {markdownSnippetMarker,
        markdownReferenceMarker} from './markdown-it-meta-marker';
import {fromList, clone, forwardOne, backUntil, isAtEnd} from './zipper';
import {sliceSection, meta as metaParser} from './markdown';
import {isMetaBlock, isHeadingOpen} from './predicates';
import {mergeConcatArrays} from './utils';

// TODO: It's not good to export a mutable object. This is an issue of
//       handling global state as a singleton. Is there a better way???
//       Allow this by loosening the linter at the top of the file.
export let md = markdownIt().use(markdownMetaBlock)
                            .use(markdownSnippetMarker)
                            .use(markdownReferenceMarker);

export const tokenize = src => md.parse(src);

// Meta attribute handler.
const metaHandler = _.curry((value, tokens) => {
  const zipper = backUntil(isHeadingOpen, clone(tokens));
  const section = sliceSection(zipper);

  return {[value]: [section]};
});

const arrayify = _.curry((fn, value, zipper) =>
  _.reduce((memo, e) =>
    _.merge(memo, fn(_.trim(e), zipper))
  , {}, _.split(',', value)));

export const uuid = metaHandler;
export const tags = arrayify(metaHandler);

const metaHandlers = {
  uuid, tags,
};

// call a handler for every meta attribute.
const parseMeta = _.curry((handlers, metaBlock, zipper) =>
  _.reduce((memo, [name, handler]) => {
    let ret = memo;
    if (_.has(name, metaBlock)) {
      const value = metaBlock[name];
      if (_.isNil(memo[name])) { memo[name] = {}; }
      ret = _.mergeWith(mergeConcatArrays,
                        memo,
                        {[name]: handler(value, zipper)});
    }
    return ret;
  }, {}, _.toPairs(handlers)));

// parse the token stream.
const parseTokens = _.curry((metaFn, zipper) => {
  let ret = {};
  let tokens = clone(zipper);

  while (!isAtEnd(tokens)) {
    const tokenType = tokens.cursor.type;
    switch (tokenType) {
      case (isMetaBlock(tokens.cursor) ? tokenType : null): {
        const metaBlock = metaParser(tokens);
        ret = _.mergeWith(mergeConcatArrays, ret, metaFn(metaBlock, tokens));
        break;
      }
      default: { break; }
    }

    tokens = forwardOne(tokens);
  }
  return ret;
});

/** Parse a markdown string.
   @param {string} src - A string in markdown format.
   @param {Object} customHandlers - Provide custom handler functions for meta
                                    block fields.
   @param {Object} meta - Attach this as meta data to the zipper.
   @return {[Zipper, Object]} - Returns a tuple containing the token stream
                                and a meta data object.
 */
export const parse = (src, customHandlers = {}, meta = {}) => {
  const handlers = _.merge(metaHandlers, customHandlers);
  const metaFn = parseMeta(handlers);
  const tokens = fromList(tokenize(src));
  tokens.meta = meta;
  const metaData = parseTokens(metaFn, tokens);

  return [tokens, metaData];
};

/** Extend the markdown-it parser with custom plugins. Any valid markdowm-it
   plugin is allowed. This function returns `nil`;
   @param {Object} plugin - The markdown-it plugin.
 */
export const extend = plugin => { md = md.use(plugin); };

export default {
  md, parse, tokenize, extend, uuid, tags,
};
