/* eslint func-names: [0], no-confusing-arrow: [2, {allowParens: true}] */
import _ from 'lodash/fp';

// Types

// The `V` functor, used for validations. It can either be of type `Valid` or
// `Invalid`.
function V() {}

function Valid(value) { this.value = value; }
Valid.prototype = Object.create(V.prototype);

function Invalid(err) { this.err = _.isArray(err) ? err : [err]; }
Invalid.prototype = Object.create(V.prototype);

V.Valid = function (value) { return new Valid(value); };
V.prototype.Valid = V.Valid;

V.Invalid = function (err) { return new Invalid(err); };
V.prototype.Invalid = V.Invalid;

V.prototype.isValid = function () { return this instanceof Valid; };
V.prototype.isInvalid = function () { return this instanceof Invalid; };

V.prototype.toString = function () {
  return this.isValid() ? this.value : this.err.join(', ');
};

// Constructors
const valid = value => new Valid(value);
const invalid = err => new Invalid(err);

// Functor
const id = valid;

const fmap = _.curry((f, instance) => {
  let ret = instance;
  if (instance.isValid()) { ret = valid(f(instance.value)); }
  return ret;
});

// Applicative
const pure = valid;

const ap = (left, right) => {
  let ret;

  // poor womans pattern matching.
  if (left.isInvalid() && right.isInvalid()) {
    ret = invalid(_.concat(left.err, right.err));
  } else if (left.isInvalid()) {
    ret = invalid(left.err);
  } else if (right.isInvalid()) {
    ret = invalid(right.err);
  } else if (left.isValid() && right.isValid()) {
    ret = valid(left.value(right.value));
  }

  return ret;
};

/** Apply a list of validation functions to a value.
   @param {Function[]} fs - A list of validation functions.
   @param val - The value to validate.
   @returns {Valid|Invalid}
*/
const validator = _.curry((fs, val) => {
  const n = _.size(fs);
  // To aggregate the failures, we start with a Success case containing
  // a curried function of arity N (where N is the number of validations),
  // and we just use an `apply` chain to get either the value our Success
  // function ultimately returns, or the aggregated failures.
  const nArityCurriedFn = _.reduce(memo => () => memo, val, _.range(0, n));

  return _.reduce((memo, f) => ap(memo, f(val)), valid(nArityCurriedFn), fs);
});

// Create a valid check.
// const check = _.curry((pred, err, v) => pred(v) ? new Valid(v) : new Invalid(err));
/** Create a valid check function.
   @param {Function} pred - A predicate function.
   @param {string} err - An error message if the check fails.
   @returns {Function}
*/
const check = _.curry((pred, err, v) => {
  const checked = pred(v);
  return checked ? valid(v) : invalid(err);
});

export default {
  V, Valid, Invalid, // type classes
  valid, invalid,    // constructors
  id, fmap,          // functor instance
  pure, ap,          // applicative functor instance
  validator, check,  // combinators
};
