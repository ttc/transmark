/* eslint no-param-reassign: ["error", { "props": false }] */
import _ from 'lodash/fp';

import {mergeConcatArrays} from './utils';

const mergeMeta = (left, right) =>
  _.mergeWith(mergeConcatArrays, left.meta, right.meta);

/** A single markdown token.
   @typedef {Object} Token
   @property {string} type - The token type, such as 'heading_open'.
   @property {string} content - The content of the token. */

/** A zipper structure over lists. This zipper is based on [Gérard Huet's
   functional
   pearl](http://www.st.cs.uni-sb.de/edu/seminare/2005/advanced-fp/docs/huet-zipper.pdf).
 */
export class Zipper {
  /**
     @param {Array} list - The RHS of the zipper.
     @param {Array} path - The LHS of the zipper. */
  constructor(list = [], path = [], meta = {}) {
    this.list = list;
    this.path = path;
    this.meta = meta;
  }

  /** The total size of the zipper.
     @type {number}
     @return {number} */
  get length() { return _.size(this.path) + _.size(this.list); }

  /** The current position.
     @type {number}
     @returns {number} */
  get index() { return _.size(this.path); }

  /** The current element of the zipper. `cursor` is not safe, in the sense it
     can return undefined, if the zipper is at the end.

     @type {Token}
     @returns {Token} */
  get cursor() { return _.first(this.list); }
}

// Essential helpers
/** Create a clone of an existing zipper.
   @param {Zipper} zipper - The zipper to clone.
   @returns {Zipper} - The cloned zipper. */
export const clone = zipper => new Zipper(zipper.list, zipper.path, zipper.meta);

// Functor
export const fmap = _.curry((fn, zipper) => {
  const list = _.map(fn, zipper.list);
  const path = _.map(fn, zipper.path);
  return new Zipper(list, path);
});

// Monoid
export const empty = () => new Zipper([]);

export const append = _.curry((z1, z2) => {
  const list = _(_.reverse(z1.path))
                  .concat(z1.list)
                  .concat(_.reverse(z2.path))
                  .concat(z2.list)
                  .value();
  return new Zipper(list, [], mergeMeta(z1, z2));
});

export const concat = zs => _.reduce(append, _.first(zs), _.tail(zs));

// Applicative
export const pure = (xs, ys = [], meta = {}) => new Zipper(_.concat([], xs), ys, meta);

export const apply = _.curry((left, right) => {
  const f = left.cursor;
  const [list, path] = f([right.list, right.path]);
  return new Zipper(list, path, mergeMeta(left, right));
});

export const ap = apply;

export const sequence = _.curry((fs, zipper) =>
  _.reduce((memo, f) => apply(f, memo), zipper, fs));

// Constructors
export const id = empty;

export const fromList = list => new Zipper(list);

// Predicates
export const isAtBeginning = zipper => _.isEmpty(zipper.path);

export const isAtEnd = zipper => _.isEmpty(zipper.list);

// Traversal
// Applicative versions of `forward` and `back`.
const forwardA = pure(([list, path]) => {
  let ret;
  // is at the end.
  if (_.isEmpty(list)) {
    ret = [list, path];
  } else {
    ret = [_.drop(1, list), _.concat([_.first(list)], path)];
  }
  return ret;
});

const backA = pure(([list, path]) => {
  let ret;
  // is at the beginning.
  if (_.isEmpty(path)) {
    ret = [list, path];
  } else {
    ret = [_.concat([_.first(path)], list), _.drop(1, path)];
  }
  return ret;
});

// public traversal API
export const forwardOne = apply(forwardA);
export const backOne = apply(backA);

// TODO: Change the order of the arguments, zipper should be last.
export const forwardMany = _.curry((steps, zipper) =>
  sequence(_.times(_.constant(forwardA), steps), zipper));

export const backMany = _.curry((steps, zipper) =>
  sequence(_.times(_.constant(backA), steps), zipper));

export const forwardUntil = _.curry((pred, zipper) => {
  let z = zipper;
  do { z = apply(forwardA, z); } while (!(isAtEnd(z) || pred(z.cursor)));
  return z;
});

export const backUntil = _.curry((pred, zipper) => {
  let z = zipper;
  do { z = apply(backA, z); } while (!(isAtEnd(z) || pred(z.cursor)));
  return z;
});

export const jumpTo = _.curry((pos, zipper) => {
  const jumpTarget = pos - zipper.index;
  const fn = jumpTarget < 0 ? backMany : forwardMany;
  return fn(Math.abs(jumpTarget), zipper);
});

// Applicative versions of zipper combinators.
const startA = pure(([list, path]) => [_(path).reverse().concat(list).value()]);
const endA = pure(([list, path]) => [[_.last(list)],
                                     _(list).take(_.size(list) - 1)
                                            .reverse()
                                            .concat(path)
                                            .value()]);
const cutA = pure(([list, path]) => [_.drop(1, list), path]);
const sliceA = (from, to) => pure(([list, path]) => [_.slice(from, to, list),
                                                     path]);

// Combinators
export const start = apply(startA);
export const end = apply(endA);

export const slice = _.curry((from, to, zipper) =>
  sequence([startA, sliceA(from, to)], zipper));

export const split = zipper => [pure(_.reverse(zipper.path), [], zipper.meta),
                                pure(zipper.list, [], zipper.meta)];
export const splitAt = _.curry((pos, zipper) => _.flow([jumpTo(pos), split])(zipper));

export const cutOne = apply(cutA);
export const cutMany = _.curry((count, zipper) =>
  sequence(_.times(_.constant(cutA), count), zipper));
export const cutUntil = _.curry((pred, zipper) => {
  let z = zipper;
  do { z = apply(cutA, z); } while (!(isAtEnd(z) || pred(z.cursor)));
  return z;
});

export const insert = _.curry((source, zipper) => {
  const [head, tail] = splitAt(zipper.index, zipper);
  return jumpTo(zipper.index, concat([head, source, tail]));
});

export const replace = _.curry((source, zipper) =>
  _.flow([cutOne, insert(source)])(zipper));

export default {
  Zipper, fromList, id, clone,
  empty, fmap, append, concat, pure, apply, ap, sequence,
  isAtBeginning, isAtEnd,
  forwardOne, forwardMany, forwardUntil,
  backOne, backMany, backUntil,
  jumpTo, start, end, slice, split, splitAt, cutOne, cutMany, cutUntil,
  insert, replace,
};
