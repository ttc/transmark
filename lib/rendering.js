import {md} from './parsing';
import {cutMeta} from './markdown';
import {clone, isAtEnd, forwardOne, start} from './zipper';
import {isMetaBlock} from './predicates';

/** Render a markdown token zipper to HTML.
   @param {Zipper} tokens - The markdown token zipper.
   @return {string} The rendered HTML.
 */
export const toHtml = tokens => {
  let zipper = clone(tokens);

  while (!isAtEnd(zipper)) {
    const tokenType = zipper.cursor.type;
    switch (tokenType) {
      case (isMetaBlock(zipper.cursor) ? tokenType : null): {
        zipper = cutMeta(zipper);
        break;
      }
      default: { break; }
    }
    zipper = forwardOne(zipper);
  }
  zipper = start(zipper);

  return md.renderer.render(zipper.list, md);
};

export default {
  toHtml,
};
