import {chain, pick} from 'lodash/fp';

import parsing from './parsing';
import markdown from './markdown';
import markdownItMarker from './markdown-it-meta-marker.js';
import transformation from './transformation';
import rendering from './rendering';
import libZipper from './zipper';
import libValidation from './validation';
import libPredicates from './predicates';

const markdownFields = ['meta', 'sliceSection', 'subSections', 'cutHeading',
                        'cutMeta', 'cutHeader', 'cutMarker', 'balanceSection',
                        'insertSection', 'replaceToken', 'currentHeading',
                        'sectionTitle', 'metaSection', 'sliceHeading',
                        'forwardByHeader', 'forwardToNextSection'];
const markdownMarkerFields = ['markdownMetaMarker', 'markdownSnippetMarker'];
const parsingFields = ['parse', 'extend'];
const transformFields = ['transform', 'transformations'];
const renderFields = ['toHtml'];

const api = chain({
  zipper: libZipper,
  validation: libValidation,
  predicates: libPredicates,
}).merge(pick(markdownFields, markdown))
  .merge(pick(markdownMarkerFields, markdownItMarker))
  .merge(pick(parsingFields, parsing))
  .merge(pick(transformFields, transformation))
  .merge(pick(renderFields, rendering))
  .value();

export const parse = api.parse;
export const transform = api.transform;
export const extend = api.extend;
export const toHtml = api.toHtml;
export const meta = api.meta;
export const sliceSection = api.sliceSection;
export const sliceHeading = api.sliceHeading;
export const subSections = api.subSections;
export const balanceSection = api.balanceSection;
export const insertSection = api.insertSection;
export const replaceToken = api.replaceToken;
export const sectionTitle = api.sectionTitle;
export const currentHeading = api.currentHeading;
export const metaSection = api.metaSection;
export const cutMeta = api.cutMeta;
export const cutHeading = api.cutHeading;
export const cutHeader = api.cutHeader;
export const cutMarker = api.cutMarker;
export const forwardByHeader = api.forwardByHeader;
export const forwardToNextSection = api.forwardToNextSection;
export const markdownMetaMarker = api.markdownMetaMarker;
export const markdownSnippetMarker = api.markdownSnippetMarker;
export const zipper = api.zipper;
export const validation = api.validation;
export const predicates = api.predicates;
export const transformations = api.transformations;

export default api;
