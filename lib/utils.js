import _ from 'lodash/fp';

// customizer for mergeWith to concat arrays.
export const mergeConcatArrays = (objValue, srcValue) =>
  _.isArray(objValue) ? _.concat(objValue, srcValue) : undefined;
