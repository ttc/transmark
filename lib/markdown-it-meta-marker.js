/* eslint no-param-reassign: ["error", { "props": false }] */
const EMBED_REGEX = /^@\[(.*)\]\([\s]*(.*?)[\s]*[\)]$/im;

export const uuidLink = src => EMBED_REGEX.exec(src);

/** Create a new marker plugin for markdown-it.
   @param {string} name - The name of this marker.
   @returns {Object}
 */
export const markdownMetaMarker = name =>
  md => {
    const container = (state, startLine, endLine, silent) => {
      const pos = state.bMarks[startLine] + state.tShift[startLine];
      const end = state.bMarks[endLine];

      /* @ */
      if (state.src.charCodeAt(pos) !== 0x40) { return false; }
      /* [ */
      if (state.src.charCodeAt(pos + 1) !== 0x5B) { return false; }

      const block = state.src.substring(pos, end);
      const match = uuidLink(block);

      if (!match) { return false; }
      if (match.length < 3) { return false; }

      const label = match[1].toLowerCase();
      const uuid = match[2];

      if (label !== name) { return false; }

      if (!silent) {
        state.line = startLine + 1;
        const token = state.push(name, '', 0);
        token.map = [startLine, state.line];
        token.content = uuid;
        token.block = true;
      }

      return true;
    };

    md.block.ruler.after('blockquote', `${name}_block`, container, {
      alt: ['paragraph', 'reference', 'blockquote', 'list'],
    });
  };

/** A snippet meta marker. */
export const markdownSnippetMarker = markdownMetaMarker('snippet');

/** A reference marker */
export const markdownReferenceMarker = markdownMetaMarker('reference');

export default {
  markdownMetaMarker,
  markdownSnippetMarker,
  markdownReferenceMarker,
};
