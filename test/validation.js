import _ from 'lodash/fp';
import v from '../lib/validation';

describe('applicative validator', () => {
  describe('API', () => {
    it('has three type classes', () => {
      const validValidator = new v.Valid(23);
      const invalidValidator = new v.Invalid('some error');

      v.should.include.keys('V', 'Valid', 'Invalid');
      validValidator.should.be.instanceof(v.V);
      validValidator.should.be.instanceof(v.Valid);
      invalidValidator.should.be.instanceof(v.V);
      invalidValidator.should.be.instanceof(v.Invalid);

      validValidator.value.should.eql(23);
      invalidValidator.err.should.be.an('array');
      invalidValidator.err.should.eql(['some error']);
    });

    it('has validity predicates', () => {
      const validValidator = new v.Valid(23);
      const invalidValidator = new v.Invalid('some error');

      validValidator.isValid().should.equal(true);
      validValidator.isInvalid().should.equal(false);
      invalidValidator.isValid().should.equal(false);
      invalidValidator.isInvalid().should.equal(true);
    });

    it('has constructor functions', () => {
      const validValidator = v.valid(23);
      const invalidValidator = v.invalid('some error');

      validValidator.should.be.instanceof(v.V);
      invalidValidator.should.be.instanceof(v.V);
      validValidator.value.should.eql(23);
      invalidValidator.err.should.eql(['some error']);
    });
  });

  describe('functor instance', () => {
    it('implements fmap', () => {
      const inc = i => i + 1;
      const validValidator = new v.Valid(23);
      const invalidValidator = new v.Invalid('some error');

      v.fmap(inc, validValidator).value.should.eql(24);
      v.fmap(inc, invalidValidator).err.should.eql(['some error']);
    });

    it('satifies the first functor law', () => {
      // fmap id = id
      const validValidator = new v.Valid(23);
      const invalidValidator = new v.Invalid('some error');
      const id = _.identity;

      const lhs1 = v.fmap(_.identity, validValidator);
      const lhs2 = v.fmap(_.identity, invalidValidator);

      lhs1.value.should.eql(id(validValidator).value);
      lhs2.err.should.eql(id(invalidValidator).err);
    });

    it('satifies the second functor law', () => {
      // fmap (f . g) = fmap f . fmap g
      const validValidator = new v.Valid(23);
      const invalidValidator = new v.Invalid('some error');
      const f = a => a + 1;
      const g = a => a + 2;

      const lhs1 = _.flow([v.fmap(g), v.fmap(f)])(validValidator);
      const rhs1 = v.fmap(_.flow([g, f]), validValidator);
      const lhs2 = _.flow([v.fmap(g), v.fmap(f)])(invalidValidator);
      const rhs2 = v.fmap(_.flow([g, f]), invalidValidator);

      lhs1.value.should.eql(rhs1.value);
      lhs2.err.should.eql(rhs2.err);
    });
  });

  describe('applicative functor', () => {
    it('lifts values into the applicative', () => {
      v.pure(23).should.be.an.instanceof(v.Valid);
    });

    it('implements apply', () => {
      const inc = i => i + 1;

      const v1 = v.valid(inc);
      const v2 = v.valid(23);
      const v3 = v.invalid('some error');
      const v4 = v.invalid('some other error');

      const ap1 = v.ap(v1, v2);
      const ap2 = v.ap(v1, v3);
      const ap3 = v.ap(v4, v2);
      const ap4 = v.ap(v3, v4);

      _.forEach((e) => e.should.be.an.instanceof(v.V), [ap1, ap2, ap3, ap4]);
      ap1.value.should.equal(24);
      ap2.err.should.eql(['some error']);
      ap3.err.should.eql(['some other error']);
      ap4.err.should.eql(['some error', 'some other error']);
    });

    it('implements the identity law', () => {
      // pure id <*> x = x
      const id = _.identity;
      const x = v.valid(23);

      const lhs = v.ap(v.pure(id), x);

      lhs.value.should.equal(x.value);
    });

    it('implements the homomorphism law', () => {
      // pure f <*> pure x = pure (f x)
      const f = x => x + 1;
      const x = 23;

      const lhs = v.ap(v.valid(f), v.valid(x));
      const rhs = v.valid(f(x));

      lhs.value.should.equal(rhs.value);
    });

    it('implements the interchange law', () => {
      // u <*> pure y = pure ($ y) <*> u
      const u = x => x + 1;
      const y = 23;

      const lhs = v.ap(v.valid(u), v.valid(y));
      const rhs = v.ap(v.valid((f) => f(y)), v.valid(u));

      lhs.value.should.equal(rhs.value);
    });

    it('implements the composition law', () => {
      // pure (.) <*> x <*> y <*> z = x <*> (y <*> z)
      const comp = _.curry((f, g, x) => g(f(x)));
      const x = v.valid(a => a + 1);
      const y = v.valid(a => a + 2);
      const z = v.valid(23);

      const lhs = v.ap(v.ap(v.ap(v.pure(comp), x), y), z);
      const rhs = v.ap(x, v.ap(y, z));

      lhs.value.should.equal(rhs.value);
    });
  });

  describe('combinators', () => {
    describe('check', () => {
      it('creates checkers that return a validator', () => {
        const err = 'value is not true';
        const c = v.check(e => e === true, err);
        const success = c(true);
        const fail = c(false);

        c.should.be.a('function');
        success.should.be.an.instanceof(v.Valid);
        fail.should.be.an.instanceof(v.Invalid);
        success.value.should.eql(true);
        success.isValid().should.eql(true);
        fail.err.should.eql([err]);
        fail.isInvalid().should.eql(true);
      });
    });

    describe('validator', () => {
      it('can combine multiple checks', () => {
        const err = 'value is not true';
        const c = v.check(e => e === true, err);
        const isValid = v.validator([c, c, c]);
        const success = isValid(true);
        const fail = isValid(false);

        isValid.should.be.a('function');
        success.should.be.an.instanceof(v.Valid);
        fail.should.be.an.instanceof(v.Invalid);
        success.value.should.eql(true);
        fail.err.should.eql([err, err, err]);
      });
    });
  });
});
