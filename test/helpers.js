/* eslint global-require: 0 */
import vm from 'vm';
import chai from 'chai';

chai.should();

// Use this to run symbolic representations with modules as context.
// sandboxEval({'_': _})('_.map((i) => i + 1, [1, 2, 3])') => [2, 3, 4]
global.sandboxEval = (ctx = {}) => {
  const sandbox = vm.createContext(ctx);
  return (str) => vm.runInContext(str, sandbox);
};

// The zipper is not part of the sand box if I don't use require here.
global.zipperEval = global.sandboxEval({zipper: require('../lib/zipper')});
