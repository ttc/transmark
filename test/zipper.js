/* eslint no-param-reassign: ["error", { "props": false }] */
import _ from 'lodash/fp';
import {assert, forall, bless, random,
        constant, pair, array, nearray, tuple, nat, dict} from 'jsverify';

import zipper from '../lib/zipper';
import {model, listModel, pathModel, indexModel, cursorModel, previousCursorModel,
        zipper as symbolicZipper} from '../lib/symbolic-zipper';

const {Zipper, clone,
       forwardOne, forwardMany, forwardUntil,
       backOne, backUntil, cutUntil} = zipper;


const arbZipper = (depth = 1000, count = 5) =>
  bless({generator: () => global.zipperEval(symbolicZipper(depth, count))});

describe('List Zippers', () => {
  it('know their length', () => {
    assert(forall(arbZipper(), z => _.eq(_.size(model(z)), z.length)));
  });

  it('know their index', () => {
    assert(forall(arbZipper(), z => _.eq(indexModel(z), z.index)));
  });

  it('access the element under cursor', () => {
    assert(forall(arbZipper(), z => _.eq(cursorModel(z), z.cursor)));
  });

  it('can attach metadata to a zipper', () => {
    assert(forall(tuple([arbZipper(), dict(nat())]), ([z, meta]) => {
      z.meta = meta;
      return _.eq(z.meta, meta);
    }));
  });

  it('can create zippers from a list', () => {
    assert(forall(array(nat), (xs) =>
      _.isEqual(xs, model(zipper.fromList(xs)))));
  });

  it('can clone a zipper', () => {
    assert(forall(arbZipper(), z => _.isEqual(model(z), model(clone(z)))));
  });

  it('can traverse forward', () => {
    assert(forall(arbZipper(), z =>
      _.eq(_.nth(1, listModel(z)), zipper.forwardOne(z).cursor)));
  });

  it('can traverse multiple times forward', () => {
    assert(forall(arbZipper(), z => {
      const steps = random(1, z.length);
      return _.eq(_.nth(steps, listModel(z)), zipper.forwardMany(steps, z).cursor);
    }));
  });

  it('can traverse back', () => {
    assert(forall(arbZipper(), z =>
      _.eq(previousCursorModel(z), zipper.backOne(z).cursor)));
  });

  it('can traverse multiple times back', () => {
    assert(forall(tuple([arbZipper(), nat()]), ([z, steps]) => {
      const pos = _.size(pathModel(z)) - steps < 0 ?
                  0 :
                  _.size(pathModel(z)) - steps;
      const elem = _.nth(pos, model(z));
      return _.eq(elem, zipper.backMany(steps, z).cursor);
    }));
  });

  it('can jump to a specific position', () => {
    assert(forall(arbZipper(), z => {
      const pos = random(0, z.length - 1);
      return _.eq(_.nth(pos, model(z)), zipper.jumpTo(pos, z).cursor);
    }));
  });

  it('can jump to the beginning', () => {
    assert(forall(arbZipper(), z =>
      _.eq(_.first(model(z)), zipper.start(z).cursor)));
  });

  it('can slice a zipper', () => {
    assert(forall(arbZipper(), z => {
      const length = z.length;
      const [start, end] = _.sortBy(_.identity,
                                    [random(0, length - 1) || 0,
                                     random(0, length - 1) || 0]);
      return _.isEqual(
        _(model(z)).drop(start).take(end - start).value(),
        model(zipper.slice(start, end, z)));
    }));
  });

  it('can split into two', () => {
    assert(forall(arbZipper(), z => {
      const pos = random(0, z.length);
      return _.isEqual(
        [_.slice(0, pos, model(z)), _.slice(pos, z.length, model(z))],
        _.map(model, zipper.splitAt(pos, z)));
    }));
  });

  it('can cut elements out of a zipper', () => {
    assert(forall(arbZipper(), z => {
      const pos = z.index;
      const count = random(0, z.length - pos);
      return _.isEqual(_.concat(_.take(pos, model(z)),
                                _.drop(pos + count, model(z))),
                       model(zipper.cutMany(count, z)));
    }));
  });

  it('insert zippers after the current element', () => {
    assert(forall(tuple([arbZipper(), array(nat), array(nat)]), ([z, l, p]) => {
      const pos = z.index;
      const source = zipper.pure(l, p);
      const expected = _(_.take(pos, model(z)))
                          .concat(_.reverse(p))
                          .concat(l)
                          .concat(_.drop(pos, model(z)))
                          .value();
      return _.isEqual(expected, model(zipper.insert(source, z)));
    }));
  });

  it('replaces the current element with another list', () => {
    assert(forall(tuple([arbZipper(), array(nat), array(nat)]), ([z, l, p]) => {
      const source = zipper.pure(l, p);
      const expected = _(_.take(z.index, model(z)))
                          .concat(_.reverse(p))
                          .concat(l)
                          .concat(_.drop(z.index + 1, model(z)))
                          .value();
      return _.isEqual(expected, model(zipper.replace(source, z)));
    }));
  });

  describe('functor instance', () => {
    it('implements fmap', () => {
      const inc = (x) => x + 1;

      assert(forall(arbZipper(), z =>
        _.isEqual(_.map(inc, model(z)), model(zipper.fmap(inc, z)))));
    });

    it('holds for the first functor law', () => {
      // fmap id = id
      assert(forall(arbZipper(), z =>
        _.isEqual(_.map(_.identity, model(z)),
                  model(zipper.fmap(_.identity, z)))));
    });

    it('holds for the second functor law', () => {
      // fmap (f . g) = fmap f . fmap g
      const f = (a) => a + 1;
      const g = (a) => a + 2;

      assert(forall(arbZipper(), z => {
        const lhs = _.flow([zipper.fmap(g), zipper.fmap(f)])(z);
        const rhs = zipper.fmap(_.flow([g, f]), z);
        return _.isEqual(model(lhs), model(rhs));
      }));
    });
  });

  describe('monoid instance', () => {
    it('implements empty', () => {
      assert(forall(constant(zipper.empty()), z => _.eq(0, z.length)));
    });

    it('implements append', () => {
      assert(forall(pair(arbZipper(), arbZipper()), ([z1, z2]) =>
        _.isEqual(_.concat(model(z1), model(z2)),
                  model(zipper.append(z1, z2)))));
    });

    it('implements concat', () => {
      assert(forall(nearray(arbZipper()), (zs) =>
        _.isEqual(_.reduce((memo, z) => _.concat(memo, model(z)), [], zs),
                  model(zipper.concat(zs)))));
    });

    it('holds for the associativity law', () => {
      // (z1 mappend z2) mappend z3 = z1 mappend (z2 mappend z3)
      assert(
        forall(
          tuple([arbZipper(), arbZipper(), arbZipper()]),
          ([z1, z2, z3]) =>
            _.isEqual(
              model(zipper.append(z1,
                                  zipper.append(z2, z3))),
              model(zipper.append(zipper.append(z1, z2),
                                  z3)))));
    });

    it('holds for the left identity law', () => {
      // mempty mappend x = x
      assert(forall(arbZipper(), z =>
        _.isEqual(model(z),
                  model(zipper.append(zipper.empty(), z)))));
    });

    it('holds for the right identity law', () => {
      // x mappend mempty = x
      assert(forall(arbZipper(), z =>
        _.isEqual(model(z),
                  model(zipper.append(z, zipper.empty())))));
    });
  });

  describe('applicative functor instance', () => {
    const inc = z => ([list, path]) =>
      [_.map(x => x + z, list), _.map(x => x + z, path)];

    const inc1 = inc(1);
    const inc2 = inc(2);

    const compose = ([fs]) => {
      const f = _.first(fs);
      return [[([gs]) => {
        const g = _.first(gs);
        return [[(zs) => g(f(zs))], []];
      }], []];
    };

    it('holds for the identity law', () => {
      // pure id <*> a = a
      assert(forall(arbZipper(), z =>
        _.isEqual(model(z), model(zipper.apply(zipper.pure(_.identity), z)))));
    });

    it('holds for the homomorphism law', () => {
      // pure f <*> pure a = pure (f a)
      assert(forall(tuple([array(nat), array(nat)]), ([xs, ys]) => {
        const [list, path] = inc1([xs, ys]);
        const lhs = zipper.apply(zipper.pure(inc1), zipper.pure(xs, ys));
        const rhs = zipper.pure(list, path);
        return _.isEqual(model(lhs), model(rhs));
      }));
    });

    it('holds for the interchange law', () => {
      // u <*> pure y = pure ($ y) <*> u
      assert(forall(tuple([array(nat), array(nat)]), ([xs, ys]) => {
        const wrapper = ([list]) => {
          const f = _.first(list);
          return f([xs, ys]);
        };

        const lhs = zipper.apply(zipper.pure(inc1), zipper.pure(xs, ys));
        const rhs = zipper.apply(zipper.pure(wrapper), zipper.pure(inc1));
        return _.isEqual(model(lhs), model(rhs));
      }));
    });

    it('holds for the composition law', () => {
      // pure (.) <*> u <*> v <*> w = u <*> (v <*> w)
      assert(forall(tuple([array(nat), array(nat)]), ([xs, ys]) => {
        const u = zipper.pure(inc1);
        const v = zipper.pure(inc2);
        const w = zipper.pure(xs, ys);
        const lhs = zipper.apply(
          zipper.apply(
            zipper.apply(zipper.pure(compose), u),
            v),
          w);
        const rhs = zipper.apply(u, zipper.apply(v, w));

        return _.isEqual(model(lhs), model(rhs));
      }));
    });
  });
});

describe('List zippers', () => {
  let zz;

  beforeEach(() => {
    zz = new Zipper([1, 2, 3, 4, 5]);
  });

  describe('traversal operations', () => {
    it('stops to traverse to the back if the cursor is at the beginning', () => {
      zz = backOne(zz);
      zz.list.should.eql([1, 2, 3, 4, 5]);
      zz.path.should.eql([]);
    });

    it('stops to traverse forward if the cursor is at the end', () => {
      zz = forwardMany(5, zz);
      zz.list.should.eql([]);
      zz.path.should.eql([5, 4, 3, 2, 1]);
      zz = forwardOne(zz);
      zz.list.should.eql([]);
      zz.path.should.eql([5, 4, 3, 2, 1]);
    });

    it('can traverse forward until a predicate holds true', () => {
      zz = forwardUntil((e) => e === 3, zz);
      zz.cursor.should.equal(3);
      zz.list.should.eql([3, 4, 5]);
      zz.path.should.eql([2, 1]);
    });

    it('can traverse back until a predicate holds true', () => {
      zz = forwardMany(4, zz); // ([4,3,2,1], [5]);
      zz = backUntil((e) => e === 3, zz);
      zz.cursor.should.equal(3);
      zz.list.should.eql([3, 4, 5]);
      zz.path.should.eql([2, 1]);
    });

    it('can cut until a predicate holds true', () => {
      zz = cutUntil(e => e === 3, zz);
      zz.cursor.should.equal(3);
      zz.list.should.eql([3, 4, 5]);
      zz.path.should.eql([]);
    });
  });
});
