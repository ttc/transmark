import markdownIt from 'markdown-it';
import yaml from 'js-yaml';

import {a} from './markdown-data-builder';
import {markdownMetaBlock} from '../lib/markdown-it-meta.js';

describe('Markdown-it meta block plugin', () => {
  let md;

  beforeEach(() => {
    md = markdownIt().use(markdownMetaBlock);
  });

  it('parses a simple meta block', () => {
    const text = a.metaSection()
                  .wOpt('var', 'val')
                  .wOpt('var2', 'val2')
                  .build();
    const tokens = md.parse(text);

    tokens.length.should.eql(1);
    tokens[0].content.should.equal('var: val\nvar2: val2');
  });

  it('parses a meta block in a bigger document', () => {
    const text = a.document()
                  .w(a.heading().wTitle('First Section'))
                  .w(a.metaSection().wOpt('var', 'val'))
                  .w(a.heading(2).wTitle('Second Heading'))
                  .build();
    const tokens = md.parse(text);

    tokens[3].type.should.equal('meta_block');
    tokens[3].content.should.equal('var: val');
  });

  it('accepts an optional params attribute', () => {
    const tokensWithoutParams = md.parse(['---',
                                          '---'].join('\n'));
    const tokensWithParams = md.parse(['--- meta',
                                       '---'].join('\n'));

    tokensWithParams[0].params.should.equal('meta');
    tokensWithoutParams[0].params.should.equal('');
  });

  it('parses yaml sequences', () => {
    const tokens1 = md.parse(['---',
                              'val:',
                              '  - 42',
                              '  - 23',
                              '---'].join('\n'));
    const tokens2 = md.parse(['---',
                              'val: [42, 23]',
                              '---'].join('\n'));
    const parsed1 = yaml.safeLoad(tokens1[0].content);
    const parsed2 = yaml.safeLoad(tokens2[0].content);

    parsed1.val.should.eql([42, 23]);
    parsed1.should.eql(parsed2);
  });

  it('parses yaml mappings', () => {
    const tokens1 = md.parse(['---',
                              'val:',
                              '  a: 42',
                              '  b: 23',
                              '---'].join('\n'));
    const tokens2 = md.parse(['---',
                              'val: {a: 42, b: 23}',
                              '---'].join('\n'));
    const parsed1 = yaml.safeLoad(tokens1[0].content);
    const parsed2 = yaml.safeLoad(tokens2[0].content);

    parsed1.val.should.eql({a: 42, b: 23});
    parsed1.should.eql(parsed2);
  });
});
