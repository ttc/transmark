 /* eslint default-case: [0], consistent-return: [0] */
import _ from 'lodash/fp';
import sinon from 'sinon';
import jsc from 'jsverify';
import {Zipper} from '../lib/zipper';
import {model, pathModel, listModel, indexModel, cursorModel, previousCursorModel,
        zipperConstructor, zipper} from '../lib/symbolic-zipper';

describe('zipper symbolic representations', () => {
  let randomizer;

  beforeEach(() => { randomizer = sinon.stub(jsc, 'random'); });

  afterEach(() => { jsc.random.restore(); });

  describe('constructor', () => {
    it('can return an empty zipper', () => {
      randomizer.returns(0);

      zipperConstructor().should.equal('zipper.empty()');
    });

    it('can return a non empty zipper', () => {
      randomizer.onCall(0).returns(1)
                .onCall(1).returns(23)
                .onCall(2).returns(42);

      zipperConstructor(2).should.equal('zipper.fromList([23, 42])');
    });
  });

  describe('traversal', () => {
    it('moves forward', () => {
      randomizer.onCall(0).returns(1)  // forward
                .onCall(1).returns(0)  // constructor
                .onCall(2).returns(0); // empty

      zipper(10).should.equal('zipper.forwardOne(zipper.empty())');
    });

    it('moves back', () => {
      randomizer.onCall(0).returns(2)  // back
                .onCall(1).returns(0)  // constructor
                .onCall(2).returns(0); // empty

      zipper(10).should.equal('zipper.backOne(zipper.empty())');
    });

    it('jumps to a position', () => {
      randomizer.onCall(0).returns(3)  // jumpTo
                .onCall(1).returns(5)  // position
                .onCall(2).returns(0)  // constructor
                .onCall(3).returns(0); // empty

      zipper(10).should.equal('zipper.jumpTo(5, zipper.empty())');
    });

    it('moves back to the start', () => {
      randomizer.onCall(0).returns(4)  // start
                .onCall(1).returns(0)  // constructor
                .onCall(2).returns(0); // empty

      zipper().should.equal('zipper.start(zipper.empty())');
    });

    it('moves forward to the end', () => {
      randomizer.onCall(0).returns(5)  // end
                .onCall(1).returns(0)  // constructor
                .onCall(2).returns(0); // empty

      zipper().should.equal('zipper.end(zipper.empty())');
    });

    it('recurses depending on the depth', () => {
      randomizer.onCall(0).returns(2)  // back
              .onCall(1).returns(1)  // forward
              .onCall(2).returns(1)  // forward
              .onCall(3).returns(0); // empty
      const string = ['zipper.backOne(',
                      'zipper.forwardOne(',
                      'zipper.forwardOne(',
                      'zipper.empty()',
                      ')',
                      ')',
                      ')'].join('');
      zipper(3).should.equal(string);
    });
  });

  describe('Model functions', () => {
    jsc.property('path model', 'array nat', (xs) =>
      _.isEqual(_.reverse(xs), pathModel(new Zipper([], xs))));

    jsc.property('list model', 'array nat', (xs) =>
      _.isEqual(xs, listModel(new Zipper(xs))));

    jsc.property('zipper model', 'array nat & array nat', ([xs, ys]) =>
      _.isEqual(_.concat(_.reverse(ys), xs), model(new Zipper(xs, ys))));

    jsc.property('index model', 'array nat & array nat', ([xs, ys]) =>
      _.isEqual(_.size(ys), indexModel(new Zipper(xs, ys))));

    jsc.property('cursor model', 'array nat & array nat', ([xs, ys]) =>
      _.isEqual(_.first(xs), cursorModel(new Zipper(xs, ys))));

    jsc.property('previous cursor model', 'array nat & array nat', ([xs, ys]) => {
      const zs = _.concat(_.reverse(ys), xs);
      const pos = ys.length === 0 ? 0 : ys.length - 1;
      return _.isEqual(_.nth(pos, zs), previousCursorModel(new Zipper(xs, ys)));
    });
  });
});
