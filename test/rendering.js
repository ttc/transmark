import {parse} from '../lib/parsing';
import {toHtml} from '../lib/rendering';

import {a} from './markdown-data-builder';

describe('transformations', () => {
  it('can render to html', () => {
    const text = a.document()
                  .w(a.heading().wTitle('First section'))
                  .w(a.heading(2).wTitle('Second section'))
                  .w(a.metaSection().wUuid('123'))
                  .w(a.paragraph().wText('A paragraph.'))
                  .w(a.heading(2).wTitle('Another second section'))
                  .w(a.paragraph().wText('Some other paragraph.'))
                  .build();
    const [zipper] = parse(text);
    const html = toHtml(zipper);

    html.should.equal(`<h1>First section</h1>
<h2>Second section</h2>
<p>A paragraph.</p>
<h2>Another second section</h2>
<p>Some other paragraph.</p>
`);
  });
});
