import _ from 'lodash/fp';

const token = (type, opts = {}) => _.merge({type, content: ''}, opts);

class Document {
  constructor() {
    this.rows = [];
    this.objects = [];
  }
  w(val) {
    this.objects = _.concat(this.objects, val);
    this.rows = _.concat(this.rows, val.build());
    return this;
  }
  get tokenTypes() { return []; }
  tokens() { return _.map(token, this.tokenTypes); }
  build() { return _.join('\n', this.rows); }
  tokenize() {
    const tokens = _.reduce((memo, obj) =>
      _.concat(memo, obj.tokenize())
    , this.tokens(), this.objects);
    return tokens;
  }
}

class Heading extends Document {
  constructor(headingLevel) {
    super();
    this.level = headingLevel;
  }
  get tokenTypes() { return ['heading_open', 'inline', 'heading_close']; }
  wTitle(title) {
    const pounds = _.join('', _.times(_.constant('#'), this.level));
    this.rows = _.concat(this.rows, `${pounds} ${title}`);
    return this;
  }
}

class Paragraph extends Document {
  get tokenTypes() { return ['paragraph_open', 'inline', 'paragraph_close']; }
  wText(text) {
    this.rows = _.concat(this.rows, _.split('\n', text));
    return this;
  }
}

class MetaSection extends Document {
  constructor() {
    super();
    this.opts = [];
  }
  get tokenTypes() {
    return ['container_meta_open',
            'paragraph_open',
            'inline',
            'paragraph_close',
            'container_meta_close'];
  }
  wOpt(key, value) {
    this.opts = _.concat(this.opts, [[key, value]]);
    return this;
  }
  wUuid(value) {
    this.wOpt('uuid', value);
    return this;
  }
  build() {
    return _(['--- meta'])
      .concat(_.map(([k, v]) => {
        const val = _.isArray(v) ? `[${_.join(',', v)}]` : v;
        return `${k}: ${val}`;
      }, this.opts))
      .concat('---')
      .join('\n');
  }
}

class MetaMarker extends Document {
  get tokenTypes() {
    return [this.label];
  }
  wLabel(label) {
    this.label = label;
    return this;
  }
  wValue(value) {
    this.value = value;
    return this;
  }
  wUuid(value) {
    this.wLabel('uuid');
    this.wValue(value);
    return this;
  }
  build() { return `@[${this.label}](${this.value})`; }
}

class Snippet extends MetaMarker {
  constructor() {
    super();
    this.label = 'snippet';
  }
}

class Reference extends MetaMarker {
  constructor() {
    super();
    this.label = 'reference';
  }
}

export const a = {
  document: () => new Document(),
  heading: (level = 1) => new Heading(level),
  paragraph: () => new Paragraph(),
  metaSection: () => new MetaSection(),
  metaMarker: () => new MetaMarker(),
  snippet: () => new Snippet(),
  reference: () => new Reference(),
};

describe('markdown data builder', () => {
  it('can return an empty document', () => {
    a.document().build().should.equal('');
  });

  it('can construct markdown documents with a heading', () => {
    const md = a.document().w(a.heading().wTitle('A Title')).build();
    md.should.equal('# A Title');
  });

  it('can construct markdown documents with a sub heading', () => {
    const md = a.document().w(a.heading(3).wTitle('A Title')).build();
    md.should.equal('### A Title');
  });

  it('can construct documents with a paragraph', () => {
    const md = a.document().w(a.paragraph().wText('Some text.')).build();
    md.should.equal('Some text.');
  });

  it('can construct documents with a multiline paragraph', () => {
    const text = `Hello World
The next line.`;
    const md = a.document().w(a.paragraph().wText(text)).build();
    md.should.equal(`Hello World
The next line.`);
  });

  it('can render meta sections', () => {
    const md = a.document()
                .w(a.metaSection()
                    .wUuid('123')
                    .wOpt('key', 'value')
                    .wOpt('ary', [23, 42]))
                .build();
    md.should.equal(`--- meta
uuid: 123
key: value
ary: [23,42]
---`);
  });

  it('can render meta markers', () => {
    const md = a.document()
                .w(a.metaMarker().wLabel('haha').wValue('huhu'))
                .build();
    md.should.equal('@[haha](huhu)');
  });

  it('can render uuid meta markers', () => {
    const md = a.document().w(a.metaMarker().wUuid('123')).build();
    md.should.equal('@[uuid](123)');
  });

  it('can render snippet markers', () => {
    const md = a.document()
                .w(a.snippet().wValue('huhu'))
                .build();
    md.should.equal('@[snippet](huhu)');
  });


  it('can render reference markers', () => {
    const md = a.document()
                .w(a.reference().wValue('huhu'))
                .build();
    md.should.equal('@[reference](huhu)');
  });

  it('can construct full documents', () => {
    const md = a.document()
                .w(
                  a.heading()
                   .wTitle('Hello world')
                   .w(a.paragraph()
                       .wText('How is it?'))
                   .w(a.heading(2)
                       .wTitle('Another section')
                       .w(a.metaSection()
                           .wUuid('123'))
                       .w(a.paragraph()
                           .wText('With it\'s own paragraph.'))))
                .build();
    md.should.equal(`# Hello world
How is it?
## Another section
--- meta
uuid: 123
---
With it's own paragraph.`);
  });

  it('can tokenize an empty document', () => {
    const tokens = a.document().tokenize();
    tokens.should.eql([]);
  });

  it('can tokenize a heading', () => {
    const tokens = a.document().w(a.heading().wTitle('A heading')).tokenize();
    tokens.length.should.equal(3);
    tokens[0].type.should.equal('heading_open');
    tokens[1].type.should.equal('inline');
    tokens[2].type.should.equal('heading_close');
  });

  it('tokenize nested builder objects', () => {
    const tokens = a.document()
                    .w(a.heading().wTitle('A title'))
                    .w(a.paragraph().wText('How is it?'))
                    .tokenize();
    tokens.length.should.equal(6);
    tokens[0].type.should.equal('heading_open');
    tokens[3].type.should.equal('paragraph_open');
  });
});
