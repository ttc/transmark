import _ from 'lodash/fp';

import md from '../lib/markdown';
import {parse, tokenize, extend, uuid} from '../lib/parsing';
import {Zipper, forwardUntil, start, end} from '../lib/zipper';
import {markdownMetaMarker} from '../lib/markdown-it-meta-marker';
import {isSnippet} from '../lib/predicates';

import {a} from './markdown-data-builder';

const testTokenOrder = (tokenOrder, tokens) =>
  _.each(([t, type]) => t.type.should.equal(type), _.zip(tokens, tokenOrder));

describe('Markdown parsing', () => {
  it('can extend the markdownIt parser with custom plugins', () => {
    const excerpt = a.document()
                     .w(a.snippet().wValue('uuid1'))
                     .w(a.metaMarker().wLabel('blah').wValue('uuid2'))
                     .build();
    extend(markdownMetaMarker('blah'));
    const tokens = tokenize(excerpt);
    tokens[1].type.should.equal('blah');
    tokens[1].content.should.equal('uuid2');
  });

  it('can tokenize a markdown text', () => {
    const text = a.heading().wTitle('Hello World').build();
    const tokens = tokenize(text);
    const tokenOrder = ['heading_open',
                        'inline',
                        'heading_close'];

    tokens.length.should.equal(3);
    testTokenOrder(tokenOrder, tokens);
  });

  it('can tokenize meta blocks', () => {
    const text = a.metaSection().wUuid('123').build();
    const tokens = tokenize(text);
    const tokenOrder = ['meta_block'];

    tokens.length.should.equal(1);
    testTokenOrder(tokenOrder, tokens);
  });

  it('can parse meta blocks into objects', () => {
    const text = a.metaSection()
                  .wOpt('var', 'val')
                  .wOpt('ary', [42, 23])
                  .wUuid('123')
                  .build();

    const zipper = new Zipper(tokenize(text));
    const meta = md.meta(zipper);

    meta.should.be.an('object');
    meta.should.include.keys('var', 'ary', 'uuid');
    meta.var.should.eql('val');
    meta.ary.should.eql([42, 23]);
    meta.uuid.should.eql(123);
  });

  it('can move by a header', () => {
    const text = a.document()
                  .w(a.heading().wTitle('First Section')
                      .w(a.paragraph().wText('Content')))
                  .build();
    let zipper = new Zipper(tokenize(text));
    const tokenOrder = ['paragraph_open', 'inline', 'paragraph_close'];

    zipper = md.forwardByHeader(zipper);
    testTokenOrder(tokenOrder, zipper.list);
  });

  it('can move to the next section', () => {
    const text = a.document()
                  .w(a.heading(2).wTitle('First Section'))
                  .w(a.paragraph().wText('Content 1'))
                  .w(a.heading(3).wTitle('Subheading'))
                  .w(a.heading(2).wTitle('Second Section'))
                  .build();
    let zipper = new Zipper(tokenize(text));
    const tokenOrder = ['heading_open', 'inline', 'heading_close'];

    zipper = md.forwardToNextSection(zipper);

    testTokenOrder(tokenOrder, zipper.list);
  });

  it('can slice sections', () => {
    const text = a.document()
                  .w(a.heading().wTitle('First Section'))
                  .w(a.heading(3)
                      .wTitle('Second section')
                      .w(a.paragraph().wText('A paragraph.')))
                  .w(a.heading(2)
                      .wTitle('Another second section')
                      .w(a.paragraph().wText('Some other paragraph.')))
                  .build();
    let zipper = new Zipper(tokenize(text));
    const tokenOrder = ['heading_open',
                        'inline',
                        'heading_close',
                        'paragraph_open',
                        'inline',
                        'paragraph_close'];

    zipper = forwardUntil((e) => _.eq(e.type, 'heading_open') && _.eq(e.tag, 'h3'), zipper);
    const section = md.sliceSection(zipper);
    section.length.should.eql(6);
    testTokenOrder(tokenOrder, section.list);
  });

  it('can slice headers', () => {
    const text = a.document()
                  .w(a.heading(2)
                      .wTitle('A Section')
                      .w(a.paragraph().wText('Some text')))
                  .build();
    const zipper = new Zipper(tokenize(text));
    const tokenOrder = ['heading_open', 'inline', 'heading_close'];
    const section = md.sliceHeading(zipper);
    section.length.should.eql(3);
    testTokenOrder(tokenOrder, section.list);
  });

  it('can insert sections at point', () => {
    const section = new Zipper(
      tokenize(a.document().w(a.heading(4).wTitle('A title')).build()));
    const document = new Zipper(
      tokenize(a.document().w(a.heading().wTitle('First section')).build()));
    const tokenOrder = ['heading_open',
                        'inline',
                        'heading_close',
                        'heading_open',
                        'inline',
                        'heading_close'];

    const combinedDocument = start(md.insertSection(section, end(document)));

    testTokenOrder(tokenOrder, combinedDocument.list);
  });

  it('can replace a token at point', () => {
    const section = new Zipper(
      tokenize(a.document().w(a.heading(4).wTitle('A title')).build()));
    const document = new Zipper(
      tokenize(a.document()
                   .w(a.heading().wTitle('First section'))
                   .w(a.snippet().wValue('123'))
                   .build()));
    const tokenOrder = ['heading_open',
                        'inline',
                        'heading_close',
                        'heading_open',
                        'inline',
                        'heading_close'];

    const combinedDocument = start(
      md.replaceToken(section, forwardUntil(isSnippet, document)));

    testTokenOrder(tokenOrder, combinedDocument.list);
  });

  it('can balance a section header levels', () => {
    const section = new Zipper(
      tokenize(a.document()
                   .w(a.heading(4).wTitle('A title'))
                   .w(a.heading(5).wTitle('Another')).build()));

    let balancedSection = start(md.balanceSection('h2', section));

    balancedSection.list[0].tag.should.equal('h3');
    balancedSection.list[2].tag.should.equal('h3');
    balancedSection.list[3].tag.should.equal('h4');
    balancedSection.list[5].tag.should.equal('h4');

    balancedSection = start(md.balanceSection('h5', section));

    balancedSection.list[0].tag.should.equal('h6');
    balancedSection.list[2].tag.should.equal('h6');
    balancedSection.list[3].tag.should.equal('h7');
    balancedSection.list[5].tag.should.equal('h7');
  });

  it('can cut section contents', () => {
    const text = a.document()
                  .w(a.heading().wTitle('First Level'))
                  .w(a.heading(2).wTitle('Second Level')
                      .w(a.paragraph().wText('Content')))
                  .w(a.heading(2).wTitle(('Next Second Level')))
                  .build();
    const tokenOrder = ['heading_open', 'inline', 'heading_close',  // h1
                        'heading_open', 'inline', 'heading_close',  // h2
                        'heading_open', 'inline', 'heading_close']; // h2
    let zipper = new Zipper(tokenize(text));
    zipper = forwardUntil(e => _.eq(e.tag, 'h2'), zipper);
    zipper = start(md.cutSectionContents(zipper));
    testTokenOrder(tokenOrder, zipper.list);
  });

  it('can extract a uuid', () => {
    const text = a.document()
                  .w(a.heading().wTitle('First Section'))
                  .w(a.heading(2).wTitle('Second section'))
                  .w(a.metaSection().wUuid('123'))
                  .w(a.paragraph().wText('A paragraph.'))
                  .w(a.heading(2).wTitle('Another second section'))
                  .w(a.paragraph().wText('Some other paragraph.'))
                  .build();
    let zipper = new Zipper(tokenize(text));
    zipper = forwardUntil((e) => _.eq(e.type, 'meta_block'), zipper);
    const meta = uuid('123', zipper);

    meta['123'][0].length.should.eql(7);
  });

  it('returns a tuple with a zipper and parsed meta data', () => {
    const text = a.heading().wTitle('Hello World').build();
    const [zipper, meta] = parse(text);

    zipper.should.be.an.instanceof(Zipper);
    meta.should.be.a('object');
  });

  describe('meta blocks', () => {
    it('parses uuids and tags by default', () => {
      const text = a.document()
                    .w(a.heading().wTitle('First Section'))
                    .w(a.metaSection().wUuid('1').wOpt('tags', 'aa'))
                    .w(a.heading(2).wTitle('Second section'))
                    .w(a.metaSection()
                        .wUuid('2')
                        .wOpt('tags', 'bb, cc')
                        .wOpt('duration', '1d'))
                  .w(a.paragraph().wText('A paragraph.'))
                  .w(a.heading(2).wTitle('Another second section'))
                  .w(a.paragraph().wText('Some other paragraph.'))
                  .build();
      const [, meta] = parse(text);

      meta.should.be.an('object');
      meta.uuid.should.be.an('object');
      meta.uuid.should.have.keys('1', '2');
      meta.uuid['1'][0].should.be.an.instanceof(Zipper);
      meta.uuid['1'][0].length.should.equal(17);
      meta.uuid['2'][0].should.be.an.instanceof(Zipper);
      meta.uuid['2'][0].length.should.equal(7);
      meta.tags.should.be.an('object');
      meta.tags.should.have.keys('aa', 'bb', 'cc');
    });

    it('can extend the meta data parser with custom handlers', () => {
      const text = a.document()
                    .w(a.heading().wTitle('First Section'))
                    .w(a.metaSection().wUuid('1').wOpt('tags', 'a'))
                    .w(a.heading(2).wTitle('Second section'))
                    .w(a.metaSection()
                        .wUuid('2')
                        .wOpt('tags', 'bb, cc')
                        .wOpt('duration', '1d'))
                  .w(a.paragraph().wText('A paragraph.'))
                  .w(a.heading(2).wTitle('Another second section'))
                  .w(a.paragraph().wText('Some other paragraph.'))
                  .build();

      const duration = (value, zipper) => {
        const len = zipper.length;
        return {[value]: [len]};
      };
      const [, meta] = parse(text, {duration});

      meta.duration.should.be.an('object');
      meta.duration.should.have.keys('1d');
      meta.duration['1d'][0].should.equal(17);
    });

    it('stacks multiple data points under the same key', () => {
      const text = a.document()
                    .w(a.heading().wTitle('First Section'))
                    .w(a.metaSection().wUuid('123'))
                    .w(a.heading(2).wTitle('Second section'))
                    .w(a.metaSection().wUuid('123'))
                    .build();
      const [, meta] = parse(text);

      meta.uuid.should.have.keys('123');
      meta.uuid['123'].length.should.equal(2);
      meta.uuid['123'][0].should.be.an.instanceof(Zipper);
      meta.uuid['123'][0].length.should.equal(8);
      meta.uuid['123'][1].should.be.an.instanceof(Zipper);
      meta.uuid['123'][1].length.should.equal(4);
    });
  });
});
