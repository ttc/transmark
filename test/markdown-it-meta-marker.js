import markdownIt from 'markdown-it';

import {uuidLink,
        markdownSnippetMarker,
        markdownReferenceMarker} from '../lib/markdown-it-meta-marker';

describe('Markdown snippet extension', () => {
  let md;

  beforeEach(() => {
    md = markdownIt().use(markdownSnippetMarker, 'snippet');
  });

  it('can match uuid links', () => {
    const src = '@[snippet](uuid)';
    const match = uuidLink(src);
    match.should.be.an('array');
    match.length.should.be.gte(3);
    match[0].should.eql(src);
    match[1].should.eql('snippet');
    match[2].should.eql('uuid');

    const src2 = 'some non snippet';
    const match2 = uuidLink(src2);
    (match2 === null).should.equal(true);
  });

  it('can parse snippet links in markdown', () => {
    const markdown = '@[snippet](my-uuid)';
    const tokens = md.parse(markdown);
    tokens.length.should.eql(1);
    tokens[0].type.should.eql('snippet');
    tokens[0].content.should.eql('my-uuid');
  });
});

describe('Markdown reference extension', () => {
  let md;

  beforeEach(() => {
    md = markdownIt().use(markdownReferenceMarker, 'reference');
  });

  it('can match uuid links', () => {
    const src = '@[reference](uuid)';
    const match = uuidLink(src);
    match.should.be.an('array');
    match.length.should.be.gte(3);
    match[0].should.eql(src);
    match[1].should.eql('reference');
    match[2].should.eql('uuid');

    const src2 = 'some non reference';
    const match2 = uuidLink(src2);
    (match2 === null).should.equal(true);
  });

  it('can parse reference links in markdown', () => {
    const markdown = '@[reference](my-uuid)';
    const tokens = md.parse(markdown);
    tokens.length.should.eql(1);
    tokens[0].type.should.eql('reference');
    tokens[0].content.should.eql('my-uuid');
  });
});
