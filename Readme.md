# Transmark 3000

Transclusive markdown.

## Basics

`Transmark` allows to [transclude](https://en.wikipedia.org/wiki/Transclusion)
sections of markdown.

Given the following two markdown files:

File 1:

```markdown
# Hello Universe
## Hello World
--- meta
uuid: 123
---
Some common content.

## Hello Space
```

File 2:

```markdown
# Hello there
@[snippet](123)
```

Using transmark we can create the following document from it.

File 3:

```markdown
# Hello there

Some common content.
```

## Table of contents

-   [Installation](#installation)
-   [Usage](#usage)
-   [API](#api)
    -   [Parsing](#parsing)
    -   [Validating](#validating)
    -   [Transforming](#transforming)
    -   [Rendering](#rendering)

## Installation

```shell
npm install --save transmark
```

## Usage

```javascript
import fs from 'fs';
import {parse, transform, toHtml} from 'transmark';

// Read the markdown file.
const text = fs.readFileSync('./file.md').toString();

// Parse the raw markdown into a markdown token zipper and an object
// containing all meta data.
const [markdown, metaData] = parse(text);

// Transform the markdown token zipper into it's final representation. This
// step does all the transclusion.
const transformed = transform(metaData, markdown);

// Render the markdown token zipper to HTML.
const html = toHtml(transformed);
```

## API

### Parsing

#### parse

Parse a markdown string.

**Parameters**

-   `src` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** A string in markdown format.
-   `customHandlers` **\[[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)](default {})** Provide custom handler functions for meta
    block fields.
-   `meta` **\[[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)](default {})** Attach this as meta data to the zipper.

Returns **\[Zipper, [Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)]** Returns a tuple containing the token stream
and a meta data object.

#### extend

Extend the markdown-it parser with custom plugins. Any valid markdowm-it
plugin is allowed. This function returns `nil`;

**Parameters**

-   `plugin` **[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** The markdown-it plugin.

### Validating

#### validator

Apply a list of validation functions to a value.

**Parameters**

-   `fs` **[Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)&lt;[Function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function)>** A list of validation functions.
-   `val`  The value to validate.

Returns **(Valid | Invalid)** 

#### check

Create a valid check function.

**Parameters**

-   `pred` **[Function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function)** A predicate function.
-   `err` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** An error message if the check fails.

Returns **[Function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function)** 

### Transforming

#### transform

Transform the markdown to the final form.

**Parameters**

-   `metaData` **[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** The transmark state received from parse.
-   `customTransformers` **[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** Extend the transmark transformation.
-   `source` **Zipper** A transmark zipper to transform.

Returns **Zipper** 

### Rendering

#### toHtml

Render a markdown token zipper to HTML.

**Parameters**

-   `tokens` **Zipper** The markdown token zipper.

Returns **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** The rendered HTML.

### Markdown

#### meta

Parse the contents of a meta block using YAML.

**Parameters**

-   `tokens` **Zipper** The markdown tokens zipper.

Returns **[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** The meta data object.

#### forwardByHeader

Forward one header.

**Parameters**

-   `tokens` **Zipper** The markdonw tokens zipper.

Returns **Zipper** The zipper with the cursors moved after the header.

#### forwardToNextSection

Move the markdown token stream one section.

**Parameters**

-   `tokens` **Zipper** The markdown tokens zipper.

Returns **Zipper** The zipper with the cursor moved by one section.

#### sliceSection

Slice the section of a markdown zipper.

**Parameters**

-   `tokens` **Zipper** The zipper to slice.

Returns **Zipper** The section that got sliced.

#### sliceHeading

Slice the heading of a section.

**Parameters**

-   `tokens` **Zipper** The zipper to slice.

Returns **Zipper** The heading that got sliced.

#### subSections

Split the markdown zipper into it's subsections.

**Parameters**

-   `tokens` **Zipper** The zipper to split up.

Returns **[Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)&lt;Zipper>** The sub sections of the markdown zipper.

#### balanceSection

Balance the section levels towards a new root level.

**Parameters**

-   `rootLevel` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** The section heading tag to use as the root
    level.
-   `tokens` **Zipper** The markdown token zipper.

Returns **Zipper** The markdown token zipper with the section heading levels
rebalanced.

#### insertSection

Insert a markdown token zipper after the current element of another
zipper.

**Parameters**

-   `section` **Zipper** The zipper to insert.
-   `tokens` **Zipper** The target zipper.

Returns **Zipper** A new zipper with `section` inserted.

#### replaceToken

Replace the current token with another zipper.

**Parameters**

-   `section` **Zipper** The section that replaces.
-   `tokens` **Zipper** The zipper to replace the current token.

#### sectionTitle

Get the current section title.

**Parameters**

-   `tokens` **Zipper** A markdown token zipper.

Returns **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** The title.

#### metaSection

Parse the next meta block.

**Parameters**

-   `tokens` **Zipper** A markdown token zipper.

Returns **[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** A key-value meta object.

#### currentHeading

Get the current heading level.

**Parameters**

-   `tokens` **Zipper** A markdown token zipper.

Returns **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** The current heading tag.

#### cutHeading

Cut out a heading block.

**Parameters**

-   `tokens` **Zipper** A markdown token zipper.

Returns **Zipper** A new version of `tokens` with the heading removed.

#### cutMeta

Cut out a meta block.

**Parameters**

-   `tokens` **Zipper** A markdown token zipper.

Returns **Zipper** A new version of `tokens` with the meta block removed.

#### cutHeader

Cut out a header.

**Parameters**

-   `tokens` **Zipper** A markdown token zipper.

Returns **Zipper** A new version of `tokens` with the heading and the meta
block removed.

#### cutMarker

Cut out a meta marker.

**Parameters**

-   `tokens` **Zipper** A markdown token zipper.

Returns **Zipper** A new version of `tokens` with the meta marker removed.

#### cutSectionContents

Cut the contents of a section, but keep the header.

**Parameters**

-   `tokens` **Zipper** A markdown token zipper.

Returns **Zipper** A new version of `tokens` with the contents of the section
removed.

### Markdown-it Plugins

#### markdownMetaMarker

Create a new marker plugin for markdown-it.

**Parameters**

-   `name` **[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)** The name of this marker.

Returns **[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** 

#### markdownMetaBlock

Extend markdown-it to parse meta blocks.

**Parameters**

-   `md` **[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)** The markdown-it parser object.
-   `options` **\[[Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)](default {})** configuration options for this plugin.
    -   `options.minMarkers` **\[[number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)]** The number or marker strings to
        delimit the block. (optional, default `3`)
    -   `options.markerStr` **\[[string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)]** The string to use as a delimiter. (optional, default `-`)

#### markdownSnippetMarker

A snippet meta marker.

#### markdownReferenceMarker

A reference marker
